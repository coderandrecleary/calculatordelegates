﻿using System;

namespace Calculator
{
    class Program
    {
        public delegate void pointerFunction()
; static void Main(string[] args)
        {
            Program s = new Program();


            pointerFunction obj = new pointerFunction(s.Calc);
            obj.Invoke();
        }


        public int Add(int a, int b)
        {
            int sum = a + b;
            return sum;

        }

        public int Subtract(int x, int y)
        {
            int subtract = x - y;
            return subtract;

        }


        public int Divide(int c, int e)
        {
            int division = c / e;
            return division;
        }


        public int Multiply(int w, int z)
        {
            int multiply = w * z;
            return multiply;
        }


        public void Calc()
        {
            while (true)
            {
                Console.WriteLine("What function would you like to use?");
                string input = Console.ReadLine();
                if (input == "Add" || input == "add")
                {
                    Console.WriteLine("Please enter your first number:");
                    int num_one = int.Parse(Console.ReadLine());

                    Console.WriteLine("Please enter your second number:");
                    int num_two = int.Parse(Console.ReadLine());
                    int output = Add(num_one, num_two);
                    Console.WriteLine(output);
                }
                else if (input == "Subtract" || input == "subtract")
                {
                    Console.WriteLine("Please enter your first number:");
                    int num_one = int.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter your second number:");
                    int num_two = int.Parse(Console.ReadLine());
                    int output = Subtract(num_one, num_two);
                    Console.WriteLine(output);
                }
                else if (input == "Divide" || input == "divide")
                {
                    Console.WriteLine("Please enter your first number:");
                    int num_one = int.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter your second number:");
                    int num_two = int.Parse(Console.ReadLine());
                    int output = Divide(num_one, num_two);
                    Console.WriteLine(output);
                }
                else if (input == "Multiply" || input == "multiply")
                {
                    Console.WriteLine("Please enter your first number:");
                    int num_one = int.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter your second number:");
                    int num_two = int.Parse(Console.ReadLine());
                    int output = Multiply(num_one, num_two);
                    Console.WriteLine(output);
                }
                else if (input == "No" || input == "exit" || input == "q" || input == "quit" || input == "no")
                {
                    break;
                }

            }


        }





    }
}
